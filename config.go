package main

import (
	"encoding/json"
	"io/ioutil"
)

var Conf Configure

type Configure struct {
	Offset    int32    `json:"offset"`
	DefOffset int32    `json:"defOffset"`
	Addrs     []string `json:"addrs"`
	TPs       []struct {
		Topic     string `json:"topic"`
		Partition int32  `json:"partition"`
	} `json:"topics"`
}

func load_conf() {
	//read config
	jsonstr, err := ioutil.ReadFile("./conf/kafka.json")
	if err != nil {
		panic(err)
	}
	//parse json
	if err := json.Unmarshal(jsonstr, &Conf); err != nil {
		panic(err)
	}
	if len(Conf.TPs) == 0 {
		panic(ErrNoTopicInConfErr)
	}
	//to map
	for _, tp := range Conf.TPs {
		//TPMap[tp.Topic] = tp.Partition
		TopicDistriMap[tp.Topic] = &TopicSubs{
			topic:        tp.Topic,
			partitionNum: tp.Partition,
		}
	}
	//read offset
	read_offset()
}
