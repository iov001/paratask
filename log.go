// +build linux

package main

import (
	//"fmt"
	"os"
	"os/signal"
	"paratask/modules/mconfig"
	"paratask/modules/mlog"
	//"path/filepath"
	"syscall"
)

func logInit() {
	level, err := mconfig.Conf.String("normal", "LogLevel")
	if err != nil {
		level = "debug"
	}
	mlog.SetLogLevel(DebugLevel(level).TransLevel())
	DebugLevel(level).SynGetLevel()
	mlog.BeeLogger.Async()
}

type DebugLevel string

func (me DebugLevel) SynGetLevel() {
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGUSR2)
	go func() {
		for {
			<-s

			mconfig.Reload()
			level, err := mconfig.Conf.String("normal", "LogLevel")
			if err != nil {
				level = "debug"
			}
			mlog.Info("Debug new level is: ", level)
			mlog.SetLogLevel(DebugLevel(level).TransLevel())
		}
	}()
}

func (me DebugLevel) TransLevel() int {
	switch string(me) {
	case "emergency":
		return mlog.LevelEmergency
	case "alert":
		return mlog.LevelAlert
	case "critical":
		return mlog.LevelCritical
	case "error":
		return mlog.LevelError
	case "warning":
		return mlog.LevelWarning
	case "notice":
		return mlog.LevelNotice
	case "information":
		return mlog.LevelInformational
	case "debug":
		return mlog.LevelDebug
	default:
		return mlog.LevelDebug
	}
}
