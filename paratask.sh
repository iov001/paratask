#!/bin/bash
#
# chkconfig: 23456 91 91
# description: APT_PRE
# processname: apt_pre
#
. /etc/profile
APP="paratask"
WORK_DIR="${APT_HOME}/paratask"

start() {
	# start preprocess 
    cd ${WORK_DIR}
    ./paratask &> /dev/null &
	# save pid
    echo $! > ${APT_PID_FILE}/${APP}.pid
}

stop() {
   # kill preprocess 
   local pids=$(pgrep paratask)
   for pid in ${pids};
   do
        kill -9 ${pid} 
   done
}

restart() {
	echo ""
}

main() {
    case $1 in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        restart
    ;;
    *)
        echo "start|stop|restart|test"
    ;;
	esac	
}

main "$@"
