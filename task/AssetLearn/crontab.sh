#!/bin/bash

. /etc/profile

CRONTAB=/var/spool/cron/root
COLLECT_SH="${APT_HOME}/paratask/task/AssetLearn/attack_collect.sh"
tempstring="#begin-of-file"
collect_sh_string="*/5 * * * * ${COLLECT_SH}"

### get configuration info
l_hostname=$(apt_config_show mysql host     2>/dev/null |sed 's/;/,/g')
l_username=$(apt_config_show mysql username 2>/dev/null |sed 's/;/,/g')
l_password=$(apt_config_show mysql password 2>/dev/null |sed 's/;/,/g')
l_dbname=$(apt_config_show mysql dbname   2>/dev/null |sed 's/;/,/g')

### config the scala sh
sed -i "/mysql -h/c mysql -h ${l_hostname} -u ${l_username} --password=${l_password} << EOF" ${COLLECT_SH}
sed -i "/use /c use ${l_dbname};" ${COLLECT_SH}

if [[ ! -f "$CRONTAB" ]] || [[ ! -s "$CRONTAB" ]];then
	echo "${tempstring}" > $CRONTAB
fi

sed -i '/.*attack_collect.*/d' ${CRONTAB}

echo "$collect_sh_string" >> ${CRONTAB}

service crond restart
