package AssetLearn

import (
	"bytes"
	//"database/sql"
	"errors"
	"strings"
	//"fmt"
	"net"
	"paratask/modules/mlog"
	"time"

	useragent_pk "github.com/xojoc/useragent"
)

var Ch = make(chan interface{}, 10000)

var switch_flag = 0

func timer_to_mysql() {
	timer := time.NewTimer(time.Duration(ToMysqlInterval) * time.Second)
	for {
		<-timer.C

		if switch_flag == 0 {
			switch_flag = 1
			ip_os_p = &ip_oss[1]
			ip_port_pad_p = &ip_port_pads[switch_flag]

		} else {
			switch_flag = 0
			ip_os_p = &ip_oss[0]
			ip_port_pad_p = &ip_port_pads[switch_flag]
		}
		trans_to_mysql()
		timer.Reset(time.Duration(ToMysqlInterval) * time.Second)
	}
}
func trans_to_mysql() {
	index := 0
	if switch_flag == 0 {
		index = 1
	}
	new(assetServer).Clean()
	mlog.Debug("len ip_oss=", len(ip_oss[index]))
	var ass = make([]*assetServer, 0, len(ip_oss[index]))
	for ip, os := range ip_oss[index] {
		if port_pad, exist := ip_port_pads[index][ip]; exist {
			for port, proto := range port_pad {
				as := &assetServer{
					ip:     ip,
					ostype: os,
					port:   port,
					proto:  proto,
				}
				ass = append(ass, as)
			}
		} else {
			as := &assetServer{
				ip:     ip,
				ostype: os,
			}
			ass = append(ass, as)
		}
	}
	new(assetServer).InsertMulti(ass)

	ip_port_pads[index] = map[string]map[int32]string{}
	ip_oss[index] = map[string]string{}
}
func trans_to_mysql_2() {
	index := 0
	if switch_flag == 0 {
		index = 1
	}
	new(assetServer).Clean()
	mlog.Debug("len ip_port_pad=", len(ip_port_pads[index]))
	for ip, port_pad := range ip_port_pads[index] {
		os := ""
		if v, ok := ip_oss[index][ip]; ok {
			os = v
		}
		var ass = make([]*assetServer, 0, len(port_pad))
		for port, _ := range port_pad {
			as := &assetServer{
				ip:     ip,
				ostype: os,
				port:   port,
			}
			ass = append(ass, as)
		}
		new(assetServer).InsertMulti(ass)
	}
	ip_port_pads[index] = map[string]map[int32]string{}
	ip_oss[index] = map[string]string{}
}

type iprange struct {
	Start net.IP
	End   net.IP
}
type iprangeSlice []iprange

var ipranges iprangeSlice = make([]iprange, 4)

func (this iprangeSlice) checkIpInRange(ip string) (error, bool) {
	//check iprange
	trial := net.ParseIP(ip)
	if trial.To4() == nil {
		mlog.Warn("sip [", ip, "] is not ipv4")
		return ErrNotIpv4Form, false
	}
	var notInRange = true
	for _, iprange := range ipranges {
		if bytes.Compare(trial, iprange.Start) >= 0 && bytes.Compare(trial, iprange.End) <= 0 {
			notInRange = false
		}
	}
	if notInRange {
		return nil, false
	}
	return nil, true
}

func init_ip_range() {
	ipr := iprange{}
	ipr.Start = net.ParseIP("10.0.0.0")
	ipr.End = net.ParseIP("10.255.255.255")
	ipranges = append(ipranges, ipr)

	ipr.Start = net.ParseIP("172.16.0.0")
	ipr.End = net.ParseIP("172.31.255.255")
	ipranges = append(ipranges, ipr)

	ipr.Start = net.ParseIP("192.168.0.0")
	ipr.End = net.ParseIP("192.168.255.255")
	ipranges = append(ipranges, ipr)
	return
}

type Conn struct {
	Proto uint8  `json:Proto`
	Sport uint16 `json:Sport`
	Dport uint16 `json:Dport`
	Sip   string `json:Sip`
	Dip   string `json:Dip`
}

type learnSrcInfo struct {
	conn      Conn
	useragent string
}

func Learn(conn Conn, useragent string) {
	data := &learnSrcInfo{
		conn:      conn,
		useragent: useragent,
	}
	Ch <- data
	return
}

var ip_oss = [2]map[string]string{map[string]string{}, map[string]string{}}
var ip_os_p *map[string]string = &ip_oss[switch_flag]

func learn_srcip(srcip string, useragent string) error {
	if useragent == "" {
		return nil
	}
	//check if in iprange
	err, ok := ipranges.checkIpInRange(srcip)
	if err != nil || !ok {
		return nil
	}
	mlog.Debug("srcip=", srcip)
	//parse useragent
	ua := useragent_pk.Parse(useragent)
	if ua == nil {
		//mlog.Warn("useragent=", useragent, "ua==nil")
		return nil
	}
	os := ua.OS
	mlog.Debug("srcip=", srcip, " os=", os)
	//mlog.Debug("os=", os)
	if os == "" {
		return nil
	}

	//save
	ip_os := *ip_os_p
	os = change_os_form(os)
	if !check_os(os) {
		return nil
	}
	ip_os[srcip] = os
	return nil
}

func check_os(os string) bool {
	if os == "android" || os == "ios" {
		return false
	}
	return true
}

func change_os_form(os string) string {
	os = strings.ToLower(os)
	if os == "android" || os == "gnu/linux" {
		os = "linux"
	}
	if os == "mac os x" {
		os = "mac"
	}
	return os
}

var ip_port_pads = [2]map[string]map[int32]string{map[string]map[int32]string{}, map[string]map[int32]string{}}
var ip_port_pad_p *map[string]map[int32]string = &ip_port_pads[switch_flag]

func learn_dstip(conn Conn) error {
	//chech if in iprange
	err, ok := ipranges.checkIpInRange(conn.Dip)
	if err != nil || !ok {
		return nil
	}
	//check if dport is well-known
	if _, wellKnown := WellKnownPorts[int32(conn.Dport)]; !wellKnown {
		//需要保存吗??
		//mlog.Warn("dip:", conn.Dip, " dport:", conn.Dport, " is not wellkonwn")
		return nil
	}
	//mlog.Debug("dstIp ", conn.Dip, " open port ", conn.Dport)
	//save
	ip_port_pad := *ip_port_pad_p
	if _, exist := ip_port_pad[conn.Dip]; !exist {
		portsmap := map[int32]string{}
		ip_port_pad[conn.Dip] = portsmap
	}
	proto := ""
	if conn.Proto == 6 {
		proto = "tcp"
	} else if conn.Proto == 17 {
		proto = "udp"
	}
	ip_port_pad[conn.Dip][int32(conn.Dport)] = proto
	return nil
}

func LearnIp(conn Conn, useragent string) error {
	//learn src ip
	learn_srcip(conn.Sip, useragent)
	//learn dst ip
	learn_dstip(conn)
	//return
	return nil
}

var ErrNotIpv4Form = errors.New("is not ipv4 form")
