package AssetLearn

import (
	"database/sql"
	"fmt"
	"paratask/modules/mconfig"
	"paratask/modules/mlog"
	"strings"
	"unicode"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

type dbInfo struct {
	User     string
	Password string
	Host     string
	Port     string
	DbName   string
}

func init_mysql() {
	dbInfo := &dbInfo{
		User:     ConfString("assetDB", "DbUser"),
		Password: ConfString("assetDB", "DbPassword"),
		Host:     ConfString("assetDB", "DbHost"),
		Port:     ConfString("assetDB", "DbPort"),
		DbName:   ConfString("assetDB", "DbName"),
	}
	db = dbInfo.Connect()
	//create table
	CreateTables(new(assetServer))
}

func (this *dbInfo) Connect() *sql.DB {
	dbUrl := this.User + ":" + this.Password + "@tcp(" + this.Host + ":" + this.Port + ")/" + this.DbName + "?charset=utf8&&loc=Asia%2FShanghai"
	mlog.Debug("dburl=", dbUrl, "Dbhost=", this.Host)
	Db, err := sql.Open("mysql", dbUrl)
	if err != nil {
		panic(err.Error())
	}

	Db.SetMaxOpenConns(100)
	Db.SetMaxIdleConns(50)

	err = Db.Ping()
	if err != nil {
		panic(err.Error())
	}
	return Db
}

func ConfString(db string, elem string) string {
	value, err := mconfig.Conf.String(db, elem)
	if err != nil {
		panic(fmt.Sprintf("get conf [%s]%s failed!", db, elem))
	}
	return value
}

type CreatTabler interface {
	CreatTable() error
}

func CreateTables(models ...CreatTabler) {
	for _, model := range models {
		model.CreatTable()
	}
}

func MysqlErrorNum(ret string) string {
	//return strings.Fields(ret)[1]
	return strings.FieldsFunc(ret, func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	})[1]
}
