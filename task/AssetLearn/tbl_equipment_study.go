package AssetLearn

import (
	"database/sql"
	"fmt"
	"paratask/modules/mlog"
)

type assetServer struct {
	ip     string
	ostype string
	port   int32
	proto  string
}

func (this *assetServer) TableName() string {
	return "equipment_study"
}

func (this *assetServer) Clean() error {
	sql := fmt.Sprintf(`delete from %s`, this.TableName())
	_, err := db.Exec(sql)
	if err != nil {
		mlog.Error("exec insert err:", err)
		return err
	}
	return nil
}

func (this *assetServer) InsertMulti(infos []*assetServer) error {
	if len(infos) == 0 {
		return nil
	}
	stmt, err := db.Prepare(fmt.Sprintf(`insert %s(ip,os_type,port,protocol) values(?,?,?,?)`, this.TableName()))
	if err != nil {
		mlog.Error("db.Prepare err:", err)
		return err
	}
	defer stmt.Close()

	var result sql.Result
	for _, info := range infos {
		if info.ostype == "" {
			mlog.Error("ip = ", info.ip, "os==\"\"")
			continue
		}
		mlog.Debug("insert to mysql:", info.ip)
		result, err = stmt.Exec(info.ip, info.ostype, info.port, info.proto)
		if err != nil {
			mlog.Error("exec insert err:", err)
		}
	}

	_, err = result.LastInsertId()
	if err != nil {
		mlog.Error("fetch last insert id failed:", err.Error())
		return err
	}
	return nil
}

func (this *assetServer) Get(page int32, count int32) (error, []string) {
	var ips = make([]string, 0, 100)
	var ip string
	count += page
	query := fmt.Sprintf("select ip from %s LIMIT %d,%d", this.TableName(), page, count)
	rows, err := db.Query(query)
	if err != nil {
		return err, nil
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&ip)
		if err != nil {
			return err, nil
		}
		ips = append(ips, ip)
	}
	err = rows.Err()
	if err != nil {
		return err, ips
	}
	return nil, ips
}

func (this *assetServer) CreateSql() string {
	return `CREATE TABLE equipment_study(
		id   integer unsigned  AUTO_INCREMENT NOT NULL,
		ip   varchar(64) NOT NULL DEFAULT '',
		os_type   varchar(16) NOT NULL DEFAULT '',
		port   INT NOT NULL DEFAULT 0, 
		protocol   varchar(8) NOT NULL DEFAULT '',
		PRIMARY KEY (Id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
}

func (this *assetServer) CreatTable() error {
	_, err := db.Exec(this.CreateSql())

	if err != nil && MysqlErrorNum(err.Error()) == "1050" {
		mlog.Notice("Table", this.TableName(), "already exists")
		return err
	} else if err != nil {
		mlog.Debug("err:", err)
	} else {
		mlog.Notice("Create table ", this.TableName())
	}
	return nil
}
