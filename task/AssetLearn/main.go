package AssetLearn

import (
	"encoding/json"
	"os"
	"os/exec"
	"paratask/modules/mlog"
	"paratask/modules/xdr"
	"path/filepath"
)

type AssetLearn struct{}

func (this *AssetLearn) Subscribe() []string {
	return []string{"xdr", "xdr-ssl", "xdrHttp", "xdrFile"}
}

func (this *AssetLearn) Main(C chan []byte) {
	//start crontab script
	this.Crontab()
	//start ip study
	for {
		data := <-C
		xdr := xdr.XdrInfo{}
		if err := json.Unmarshal(data, &xdr); err != nil {
			mlog.Error("AssetLearn ummarshal data err:", err)
		}
		Learn(Conn(xdr.Conn), xdr.Http.UserAgent)
	}
	return
}

func (this *AssetLearn) Name() string {
	return "asset_learn"
}

func (this *AssetLearn) Crontab() {
	curdir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	var script = curdir + "/task/AssetLearn/crontab.sh"
	out, err := exec.Command(script).Output()
	if err != nil {
		mlog.Error("start crontab err:", out)
	}
	return
}
