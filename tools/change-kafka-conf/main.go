package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

type Configure struct {
	Offset    int32    `json:"offset"`
	DefOffset int32    `json:"defOffset"`
	Addrs     []string `json:"addrs"`
	TPs       []struct {
		Topic     string `json:"topic"`
		Partition int32  `json:"partition"`
	} `json:"topics"`
}

func main() {

	orig_file := flag.String("conf-file", "", "kafka conf file")
	brokers := flag.String("kafka-addrs", "", "kafka brokers with port")
	flag.Parse()

	if *orig_file == "" {
		panic("--help")
	}

	conf := new(Configure)

	//read config
	jsonstr, err := ioutil.ReadFile(*orig_file)
	if err != nil {
		panic(ErrFileNotExistErr)
	}
	//parse json
	if err := json.Unmarshal(jsonstr, conf); err != nil {
		panic(err)
	}

	addrs := strings.Split(*brokers, ";")
	conf.Addrs = addrs

	jsonstr, err = json.Marshal(conf)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(jsonstr))
	return
}

var ErrFileNotExistErr = errors.New("file not exist")
var ErrParseJsonErr = errors.New("parse json error")
