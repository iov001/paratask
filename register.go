package main

import (
	"paratask/task/AssetLearn"
)

var ParaTaskers []ParaTasker

type ParaTasker interface {
	Main(C chan []byte)
	Subscribe() []string
	Name() string
}

func register_task(pt ParaTasker) {
	ParaTaskers = append(ParaTaskers, pt)
	return
}

func register() {
	register_task(new(AssetLearn.AssetLearn))
}
