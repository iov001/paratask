#!/bin/bash
tomorrow_date=`date -d 'tomorrow' "+%Y-%m-%d"`
tomorrow_time=`date -d "$tomorrow_date" +%s`
echo $tomorrow_time
today_date=`date "+%Y-%m-%d"`
seven_day_ago_date=`date -d '6 days ago' "+%Y-%m-%d"`
seven_day_ago=`date -d "$seven_day_ago_date" +%s`
echo $seven_day_ago
mysql -h 10.82.1.16 -u root --password=mysqladmin << EOF
	use aptwebservice;
UPDATE equipment SET ids_attack_count = IFNULL((SELECT COUNT(dest_ip) FROM alert_ids WHERE alert_ids.time >= $seven_day_ago and alert_ids.time < $tomorrow_time and equipment.ip = alert_ids.dest_ip GROUP BY dest_ip),0);

UPDATE equipment SET vds_attack_count = IFNULL((SELECT COUNT(dest_ip) FROM alert_vds WHERE alert_vds.time >= $seven_day_ago and alert_vds.time < $tomorrow_time and equipment.ip = alert_vds.dest_ip GROUP BY dest_ip),0);

UPDATE equipment SET waf_attack_count = IFNULL((SELECT COUNT(hostname) FROM alert_waf WHERE alert_waf.time >= $seven_day_ago and alert_waf.time < $tomorrow_time and equipment.ip = alert_waf.hostname GROUP BY hostname),0);

UPDATE equipment SET attack_count = vds_attack_count + waf_attack_count + ids_attack_count;

replace into exceptional_visit (select ip,equipment.os_type as src_os_type,os_particular.os_type as dst_os_type,service,protocol,port,level,details from equipment left join os_particular on equipment.os_type != os_particular.os_type);

replace into brute_force (select ip,port,level,name from equipment FULL join port_list);
EOF
