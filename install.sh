#!/bin/bash

. /etc/profile
SRC_PACKAGE="${APT_PACKAGE}/paratask/paratask.tar.gz"
WORK_DIR="${APT_HOME}/paratask"
CONFIG_FILE="${WORK_DIR}/conf/app.conf"
KAFKA_CONFIG="${WORK_DIR}/conf/kafka.json"
CHANGE_KAFKA_CONF="${WORK_DIR}/tools/change-kafka-conf/change-kafka-conf"
#CUR_DIR=$(dirname $0)
CRUDINI="${WORK_DIR}/crudini"

check_env() {
	if [ ! "${APT_HOME}" ]; then
		echo "APT_HOME not exit"
		exit 1
	fi 	
	if [ ! "${APT_PACKAGE}" ]; then
		APT_PACKAGE=${APT_HOME}/package
	fi
	if [ ! "${APT_INITD}" ]; then
		APT_INITD=${APT_HOME}/etc/init.d
	fi
}

main() {
	check_env
	#unzip 
	local tar_file=${SRC_PACKAGE}
	if [ ! -f "${tar_file}" ]; then
		echo "$tar_file not exist"	
		return 1
	fi
	tar zxvf "${SRC_PACKAGE}"  -C "${APT_HOME}"

	mkdir -p "${WORK_DIR}" 

	#modify config by env
	local kafka_addrs=$(apt_config_show kafka brokers);err=$?	
	if(( err != 0 )); then
		echo "kafka brokers = ${kafka_addrs}"
		return 1
	fi 
	local addrs=`echo "${kafka_addrs}" | tr ";" "\n"`				
	local brokers=""
	for addr in $addrs; do
		brokers="${addr}:9092;${brokers}"
	done
	#get mysql config 
	local mysql_host=$(apt_config_show mysql host)
	local mysql_port=$(apt_config_show mysql port)	
	local mysql_username=$(apt_config_show mysql username)
	local mysql_password=$(apt_config_show mysql password)
	local mysql_dbname=$(apt_config_show mysql dbname)
	#change config
	if [ ! -f "${CRUDINI}" ]; then
		echo "${CRUDINI} not exist"
		return 1
	fi
	#change app.conf
	chmod +x ${CRUDINI}
	${CRUDINI} --set --existing ${CONFIG_FILE} assetDB DbHost ${mysql_host}
	${CRUDINI} --set --existing ${CONFIG_FILE} assetDB DbPort ${mysql_port}
	${CRUDINI} --set --existing ${CONFIG_FILE} assetDB DbName ${mysql_dbname}
	${CRUDINI} --set --existing ${CONFIG_FILE} assetDB DbUser ${mysql_username}
	${CRUDINI} --set --existing ${CONFIG_FILE} assetDB DbPassword ${mysql_password}

	#change kafka.json
	local json=`${CHANGE_KAFKA_CONF} --conf-file=${KAFKA_CONFIG} --kafka-addrs=${brokers}`
	echo ${json} > ${KAFKA_CONFIG}
	#generate start|stop script
	cp ${WORK_DIR}/paratask.sh ${APT_INITD}/paratask
}

main "$@"
