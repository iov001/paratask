package main

import (
	//"fmt"
	kfkc "paratask/modules/kafka_consumer"
	"paratask/modules/mlog"
	"sync"
	"time"
)

var TopicDistriMap = make(map[string]*TopicSubs)
var cli_ip_partition_outtime = make(map[*client]map[string]map[int32]*timeoutRecord)
var concur_cli_topic_partition_outtime sync.Map
var CliTimeoutMap = make(map[*client]*timeoutRecord)

func main() {
	//load conf
	load_conf()

	//register
	register()

	//prepare
	prepare()

	//start
	start()

	//safe exit
	safeExit()
	return
}

func start() {
	for _, topic := range TopicDistriMap {
		for _, pConsumer := range topic.partitionConsumers {
			go distribute(pConsumer, topic.clients)
		}
	}
	go statistic_timeout_concur()
	save_offset_to_file()
}

type timeoutRecord struct {
	count   int32
	outtime int64
}

func statistic_timeout_concur() {
	for {
		var PartitionRecord = &timeoutRecord{}
		var topicRecord = &timeoutRecord{}

		range_partition_outtime := func(key, value interface{}) bool {
			//partition := key.(int32)
			record := value.(*timeoutRecord)
			PartitionRecord.count += record.count
			PartitionRecord.outtime += record.outtime
			return true
		}

		range_topic_partition_outtime := func(key, value interface{}) bool {
			//topic := key.(string)
			//map[int32]*timeoutRecord
			partition_outtime := value.(sync.Map)
			PartitionRecord.count = 0
			PartitionRecord.outtime = 0
			partition_outtime.Range(range_partition_outtime)

			topicRecord.count += PartitionRecord.count
			topicRecord.outtime += PartitionRecord.outtime
			return true
		}

		range_cli_topic_partition_outtime := func(key, value interface{}) bool {
			cli := key.(*client)
			//map[string]map[int32]*timeoutRecord
			topic_partition_outtime := value.(sync.Map)
			topicRecord.count = 0
			topicRecord.outtime = 0
			topic_partition_outtime.Range(range_topic_partition_outtime)

			CliTimeoutMap[cli] = topicRecord
			return true
		}

		concur_cli_topic_partition_outtime.Range(range_cli_topic_partition_outtime)

		time.Sleep(60 * time.Second)
		if len(CliTimeoutMap) == 0 {
			mlog.Info("timeout tasks is 0")
		}
		for cli, timeout := range CliTimeoutMap {
			mlog.Info("task(", cli.name, ")timeout count:", timeout.count, " outtime:", timeout.outtime)
		}
	}
	return
}

/*
func statistic_timeout() {
	for {
		var PartitionRecord = &timeoutRecord{}
		var topicRecord = &timeoutRecord{}
		for cli, topicMap := range cli_ip_partition_outtime {
			for _, partitionMap := range topicMap {
				PartitionRecord.count = 0
				PartitionRecord.outtime = 0
				for _, record := range partitionMap {
					PartitionRecord.count += record.count
					PartitionRecord.outtime += record.outtime
				}
				topicRecord.count += PartitionRecord.count
				topicRecord.outtime += PartitionRecord.outtime
			}
			CliTimeoutMap[cli] = topicRecord
		}
		time.Sleep(30 * time.Second)
		for cli, timeout := range CliTimeoutMap {
			mlog.Info("task(", cli.name, ")timeout count:", timeout.count, " outtime:", timeout.outtime)
		}
	}
}
*/

func print_cli_len(clients []*client) {
	for {
		for _, client := range clients {
			mlog.Debug("client ", client.name, " ch len=", len(client.ch))
		}
		time.Sleep(30 * time.Second)
	}
}

func distribute(consumer *kfkc.ConsumerPartition, clients []*client) {
	//go print_cli_len(clients)
	timer := time.NewTimer(time.Millisecond * time.Duration(ChCliTimeout))
	for {
		//pop one data
		err, msg := consumer.Pop()
		if err != nil {
			mlog.Error(consumer.Topic, "(", consumer.Partition, ")err:", err)
		}
		//record offset to location
		save_offset(consumer.Topic, consumer.Partition, msg.Offset)
		//send to clients
		var p []*client
		var failedClis = make([]*client, 0, len(clients))
		for {
			for _, client := range clients {
				select {
				case client.ch <- msg.Value:
				default:
					failedClis = append(failedClis, client)
				}
			}

			if len(failedClis) != 0 {
				<-timer.C
				timer.Reset(time.Millisecond * time.Duration(ChCliTimeout))
				for _, failedCli := range failedClis {
					timeout_record_concur(failedCli, consumer.Topic, consumer.Partition)
				}

				p = clients
				clients = failedClis
				failedClis = p
				failedClis = failedClis[:0]
			} else {
				break
			}
		}
	}
}

/*
func distribute_2(consumer *kfkc.ConsumerPartition, clients []*client) {
	timer := time.NewTimer(time.Millisecond * time.Duration(ChCliTimeout))
	for {
		err, msg := consumer.Pop()
		if err != nil {
			mlog.Error(consumer.Topic, "(", consumer.Partition, ")err:", err)
		}
		save_offset(consumer.Topic, consumer.Partition, msg.Offset)
		for _, client := range clients {
			select {
			case client.ch <- msg.Value:
			case <-timer.C:
				timeout_record(client, consumer.Topic, consumer.Partition)
				timer.Reset(time.Millisecond * time.Duration(ChCliTimeout))
			}
		}
	}
}
*/

func timeout_record_concur(cli *client, topic string, partition int32) error {
	record := &timeoutRecord{}
	record.count = record.count + 1
	record.outtime = record.outtime + int64(ChCliTimeout)

	if topic_partition_time, exist := concur_cli_topic_partition_outtime.Load(cli); exist {
		//map[string]map[int32]*timeoutRecord
		topic_partition_time_t := topic_partition_time.(sync.Map)
		if partition_time, exist_1 := topic_partition_time_t.Load(topic); exist_1 {
			//map[int32]*timeoutRecord
			partition_time_t := partition_time.(sync.Map)
			partition_time_t.Store(partition, record)
			topic_partition_time_t.Store(topic, partition_time)
		} else {
			var partition_time_t sync.Map
			partition_time_t.Store(partition, record)
			topic_partition_time_t.Store(topic, partition_time_t)
		}
	} else {
		var partition_time sync.Map
		partition_time.Store(partition, record)
		var topic_partition_time sync.Map
		topic_partition_time.Store(topic, partition_time)
		concur_cli_topic_partition_outtime.Store(cli, topic_partition_time)
	}

	return nil
}

func timeout_record(cli *client, topic string, partition int32) error {
	record := &timeoutRecord{}
	record.count = record.count + 1
	record.outtime = record.outtime + int64(ChCliTimeout)

	if _, exist := cli_ip_partition_outtime[cli]; !exist {
		p := make(map[int32]*timeoutRecord)
		p[partition] = record
		t := make(map[string]map[int32]*timeoutRecord)
		t[topic] = p
		cli_ip_partition_outtime[cli] = t
	} else {
		if _, exist := cli_ip_partition_outtime[cli][topic]; !exist {
			p := make(map[int32]*timeoutRecord)
			p[partition] = record
			cli_ip_partition_outtime[cli][topic] = p
		} else {
			cli_ip_partition_outtime[cli][topic][partition] = record
		}
	}
	return nil
}

type TopicSubs struct {
	topic        string
	partitionNum int32
	clients      []*client
	//chCli              []chan []byte
	partitionConsumers []*kfkc.ConsumerPartition
	//partitionConsumers []*partitionConsumerInfo
}

type partitionConsumer kfkc.PartitionConsumer

type partitionConsumerInfo struct {
	partition int32
	consumer  partitionConsumer
}
type client struct {
	ch   chan []byte
	name string
}

func prepare() {
	//start topic consumer
	tconsumer := new(kfkc.TopicConsumer)
	tconsumer.Init(Conf.Addrs)

	//get cli for each topic
	for _, tasker := range ParaTaskers {
		ch := make(chan []byte, CliChanSize)
		topics := tasker.Subscribe()
		for _, topic := range topics {
			//check topic illegal
			mlog.Debug("task sub topic:", topic)
			if _, ok := TopicDistriMap[topic]; !ok {
				mlog.Error("topic " + topic + "not exist for " + tasker.Name())
				continue
			}
			//TopicDistriMap
			cli := &client{
				ch:   ch,
				name: tasker.Name(),
			}
			TopicDistriMap[topic].clients = append(TopicDistriMap[topic].clients, cli)
		}
		//get partitionConsumers
		get_partitioners(tconsumer)
		//run task
		go tasker.Main(ch)
	}
}

func get_partitioners(tconsumer *kfkc.TopicConsumer) error {
	//start partition consumer
	for _, topic := range TopicDistriMap {
		for i := 0; i < int(topic.partitionNum); i++ {
			offset := get_offset(topic.topic, i)
			pconsumer := kfkc.NewConsumerPartition(tconsumer, topic.topic, int32(i), int32(ConsumerCachSize), offset)
			if err := pconsumer.Start(); err != nil {
				mlog.Error("start "+topic.topic+" (", i, ") failed! err:"+err.Error())
				continue
			}
			mlog.Debug("start partitioner consumer: topic:", topic.topic, " partition:", i, " offset:", offset)
			topic.partitionConsumers = append(topic.partitionConsumers, pconsumer)
		}
	}
	return nil
}

func get_offset(topic string, partition int) int64 {
	if gOffset.Exist == false {
		return int64(Conf.DefOffset)
	}

	if Conf.Offset == -1 {
		return int64(-1)
	} else if Conf.Offset == -2 {
		return int64(-2)
	} else if Conf.Offset == -3 {
		for _, ttopic := range gOffset.Topics {
			if topic == ttopic.Name {
				for _, tpartition := range ttopic.Partitions {
					if tpartition.PartitionNum == int32(partition) {
						return tpartition.Offset
					}
				}
			}
		}
	}
	return int64(Conf.DefOffset)
}
