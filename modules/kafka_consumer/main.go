package kafka_consumer

import (
	"errors"
	"paratask/modules/mlog"
	"time"

	"github.com/Shopify/sarama"
)

//var Consumer = sarama.Consumer

type TopicConsumer struct {
	Consumer sarama.Consumer
	Addrs    []string
}

/**
 * addrs is in form {"ip1:9092","ip2:9092"}
 */
func (this *TopicConsumer) Init(addrs []string) error {
	this.Addrs = addrs
	var err error
	this.Consumer, err = sarama.NewConsumer(addrs, nil)
	if err != nil {
		mlog.Error("TopicConsumer.Init():", err)
		panic err
	}
	mlog.Info("start topic consumer addrs:", addrs)
	return nil
}

type PartitionConsumer sarama.PartitionConsumer

type ConsumerPartition struct {
	TopicConsumer
	Topic             string
	Partition         int32
	Offset            int64
	CachSize          int32
	Cache             *Queue
	PartitionConsumer PartitionConsumer
}

func NewConsumerPartition(
	tConsumer *TopicConsumer,
	topic string,
	partition int32,
	cachsize int32,
	offset int64,
) *ConsumerPartition {
	cp := new(ConsumerPartition)
	cp.TopicConsumer = *tConsumer
	cp.Topic = topic
	cp.Partition = partition
	cp.CachSize = cachsize
	cp.Offset = offset
	cp.Cache = new(Queue).New()
	return cp
}

func (this *ConsumerPartition) Start() error {
	if this.Consumer == nil {
		return ErrConsumerIsNil
	}
	partitionConsumer, err := this.Consumer.ConsumePartition(this.Topic, this.Partition, this.Offset)
	if err != nil {
		return err
	}
	mlog.Debug("next offset=", partitionConsumer.HighWaterMarkOffset())
	this.PartitionConsumer = PartitionConsumer(partitionConsumer)
	return nil
}

type ConsumerMessage sarama.ConsumerMessage

func (this *ConsumerPartition) Get(n int32) (error, []*ConsumerMessage) {
	results := make([]*ConsumerMessage, 0, n)

restart:
	reallen := this.Cache.Len()
	if reallen >= int(n) {
		for i := 0; i < int(n); i++ {
			_, data := this.Cache.Pop()
			result := data.(*ConsumerMessage)
			results = append(results, result)
		}
	} else if reallen > 0 {
		for i := 0; i < reallen; i++ {
			_, data := this.Cache.Pop()
			result := data.(*ConsumerMessage)
			results = append(results, result)
		}
	} else {
		timer := time.NewTimer(100 * time.Millisecond)
		for i := 0; i < int(this.CachSize); i++ {
			select {
			case msg := <-this.PartitionConsumer.Messages():
				if err := this.Cache.Push((*ConsumerMessage)(msg)); err != nil {
					mlog.Error("queue push error:", err)
				}
				//results = append(results, (*ConsumerMessage)(msg))

			case <-timer.C:
				goto outfor
				//break
				//timer.Reset(100 * time.Millisecond)
			}
		}
	outfor:
		goto restart
	}
	return nil, results
}

func (this *ConsumerPartition) Pop() (error, *ConsumerMessage) {
	err, results := this.Get(1)
	if err != nil {
		return err, nil
	}
	if len(results) == 0 {
		return nil, nil
	}
	return nil, results[0]
}

var ErrConsumerIsNil = errors.New("consumer is nil")
