package kafka_consumer

import (
	"container/list"
	"errors"
)

type Queue struct {
	List *list.List
}

func (this *Queue) New() *Queue {
	this.List = list.New()
	return this
}

func (this *Queue) Push(element interface{}) error {
	if this.List == nil {
		return ErrNotInitQueErr
	}
	this.List.PushFront(element)
	return nil
}

func (this *Queue) Pop() (error, interface{}) {
	if this.List == nil {
		return ErrNotInitQueErr, nil
	}
	if this.List.Back() == nil {
		return nil, nil
	}
	element := this.List.Back()
	this.List.Remove(element)
	return nil, element.Value
}

func (this *Queue) Len() int {
	if this.List == nil {
		return 0
	}
	return this.List.Len()
}

var ErrNotInitQueErr = errors.New("not init queue")
