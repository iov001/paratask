package main

import (
	"errors"
)

var ErrNoTopicInConfErr = errors.New("no topic in conf")
