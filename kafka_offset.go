package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"paratask/modules/mlog"
	"sync"
	"time"
)

type PartitionID struct {
	topic     string
	partition int32
}

var gOffsetMap sync.Map

//var offsetMap = map[PartitionID]int64{}
var gOffset offsetStatus

func save_offset(topic string, partition int32, offset int64) {
	id := PartitionID{
		topic:     topic,
		partition: partition,
	}

	gOffsetMap.Store(id, offset)
	return
}

func save_offset_to_file() {
	go func() {
		for {
			offsetRecord := &offsetStatus{}
			//get offset
			mapfunc := func(key, value interface{}) bool {
				rkey := key.(PartitionID)
				rvalue := value.(int64)
				partition := offsetPartition{
					PartitionNum: rkey.partition,
					Offset:       rvalue,
				}
				topic := offsetTopic{
					Name: rkey.topic,
				}
				topic.Partitions = append(topic.Partitions, partition)
				offsetRecord.Topics = append(offsetRecord.Topics, topic)
				return true
			}
			gOffsetMap.Range(mapfunc)
			//to json
			jsonstr, err := json.Marshal(offsetRecord)
			if err != nil {
				mlog.Error(offsetRecord, "marshal err:", err)
			}
			//write to local file
			err = writeFileTrunc("./offset.json", jsonstr, 0666)
			if err != nil {
				mlog.Error("write file ./offset.json err:", err)
			}
			//sleep
			time.Sleep(2 * time.Second)
		}
	}()
}

type offsetStatus struct {
	Exist  bool          `json:"-"`
	Topics []offsetTopic `json:"topics"`
}

type offsetTopic struct {
	Name       string            `json:"name"`
	Partitions []offsetPartition `json:"partitions"`
}

type offsetPartition struct {
	PartitionNum int32 `json:"partitionNum"`
	Offset       int64 `json:"offset"`
}

func read_offset() error {
	//read config
	gOffset.Exist = true
	jsonstr, err := ioutil.ReadFile("./offset.json")
	if err != nil {
		gOffset.Exist = false
		return nil
	}
	//parse json
	if err := json.Unmarshal(jsonstr, &gOffset); err != nil {
		panic("offset.json:" + err.Error())
	}
	//move offset.json
	err = os.Rename("./offset.json", "./offset.json.pre")
	if err != nil {
		mlog.Error("rename file err:", err)
	}
	return nil
}
